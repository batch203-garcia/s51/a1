import { useState } from "react";
import {Button, Card} from "react-bootstrap"

export default function CoursesCard({courseProp}){
    const {name, description, price} = courseProp;

    const [count, setCount] = useState(0);
    const [seats, setSeat] = useState(30);

    function enroll(){
        

        if(seats == 0){
            alert("No more available seats.");
            document.querySelector("#enroll").setAttribute("disabled", true);
            document.querySelector("#enroll").innerText = "FULL";
            document.querySelector("#enroll").classList.add('bg-danger');
        }else{
            setCount(count + 1);
            setSeat(seats - 1);
        }
    }

    return(
                <Card className="my-3">
                    <Card.Body>
                        <Card.Title>
                            {name}
                        </Card.Title>
                        <Card.Subtitle>
                            Description:
                        </Card.Subtitle>
                        <Card.Text>
                        {description}
                        </Card.Text>
                        <Card.Subtitle>
                            Price:
                        </Card.Subtitle>
                        <Card.Text>
                        Php {price}
                        </Card.Text>
                        <Card.Subtitle>
                            Enrollees:
                        </Card.Subtitle>
                        <Card.Text className="text-success">
                        {count} - Enrollees
                        </Card.Text>
                        <Button id="enroll" variant="primary"  onClick={enroll}>Enroll</Button>
                    </Card.Body>
                </Card>
    )
}