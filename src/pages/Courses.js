import CourseCard from "../components/CourseCard";

import coursesData from "../data/coursesData"


export default function Course(){

    const courses = coursesData.map(course => {
        return(
            <CourseCard key={course.id} courseProp={course}/>
        )
    })
    return(
        <>
        <h1>Courses</h1>
            {courses}
        </>
        
    )
}